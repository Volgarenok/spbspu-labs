#include "Queue_with_priority.hpp"
#include <cassert>//assert
#include <ctime>//random generate
#include <cstdlib>//^^^
#include <iostream>//print console 
#include <string>//item_type
#include <list>//int_list
#include <iterator>
typedef std::string item_type;
typedef std::list< int > int_list;

namespace LAB_CONST
{
	const int MAX_AMOUNT_OF_LIST = 20;
	const int MAX_RANDOM_VALUE = 20;
}


struct random_item
{
	int operator()()
	{
		return rand() % LAB_CONST::MAX_RANDOM_VALUE;
	}
};

void print_convergent_list (int_list _list)
{	
	if (_list.empty())
	{
		return;
	}
	
	struct local
	{
		static void Print(int_list::iterator forward_iterator, int_list::reverse_iterator backward_iterator, bool controller )
		{
			if (forward_iterator == backward_iterator.base())
			{
				std::cout << std::endl;
				return;
			}
			if (controller)
			{
				std::cout << *forward_iterator << " ";
				Print(++forward_iterator, backward_iterator, !controller);
			}
			else
			{
				std::cout << *backward_iterator << " ";
				Print(forward_iterator, ++backward_iterator, !controller);
			}
		}
	};		
	local::Print(_list.begin(), _list.rbegin(), true);
	return;	
}

int main ()
{
	srand(time(0));
	//List test
	int_list TEST_list;
	for(unsigned i = 0; i <= LAB_CONST::MAX_AMOUNT_OF_LIST; i++)
	{
		TEST_list;
		std::generate_n(std::insert_iterator< std::list< int > >(TEST_list, TEST_list.begin()), i, random_item());
		std::copy(TEST_list.begin(), TEST_list.end(), std::ostream_iterator< int >(std::cout, " "));
		std::cout << std::endl;
		print_convergent_list(TEST_list);
		TEST_list.clear();
	}
	
	//Queue test
	Queue_with_priority< item_type > TEST_queue;
	for(unsigned i = 0; i <= 10; i++)
	{
		if(i % 3 == 0)
		{
			TEST_queue.put("LOW");
		}
		else
		if(i % 3 == 1)
		{
			TEST_queue.put("NORMAL", TEST_queue.NORMAL);
		}
		else
		if(i % 3 == 2)
		{
			TEST_queue.put("HIGH", TEST_queue.HIGH);
		}
	}
	
	TEST_queue.accelerate();
	while (!TEST_queue.empty())
	{
		std::cout << *TEST_queue.get() << std::endl;
	}
	return 0;
}
