#ifndef SHAPE_H
#define SHAPE_H
class Shape
{
	public:
		struct point
		{
			int x;
			int y;
			point(const int new_x, const int new_y);
		};
		
		Shape(const int new_x, const int new_y);
		
		bool isMoreLeft (const Shape & shape) const;
		bool isUpper(const Shape & shape) const;
		point get() const;
		
		virtual void draw() const = 0;
	
	protected:
		point center;
		~Shape();
};
#endif//SHAPE_H
