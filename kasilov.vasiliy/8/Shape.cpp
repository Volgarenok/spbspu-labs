#include "Shape.h"
Shape::point::point(const int new_x, const int new_y):
	x(new_x),
	y(new_y)
	{
	}	

Shape::Shape(const int new_x, const int new_y):
	center(new_x, new_y)
	{
		
	}
	
Shape::point Shape::get() const
{
	return center;
}
	
bool Shape::isMoreLeft(const Shape & shape) const
{
	return center.x < shape.center.x;
}

bool Shape::isUpper(const Shape & shape) const
{
	return center.y > shape.center.y;
}

Shape::~Shape()
{
	
}
