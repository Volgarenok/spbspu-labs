#include "Circle.h"
#include "Triangle.h"
#include "Square.h"
#include "Functors_8.h"
#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <vector>
#include <iostream>
#include <iterator>
#include <cmath>
int main()
{
	srand(time(0));
	std::vector< double > sequence;
	std::generate_n(std::insert_iterator< std::vector< double > >(sequence, sequence.begin()), 5, double_generator());
	std::copy(sequence.begin(), sequence.end(), std::ostream_iterator< double >(std::cout, "\n"));
	std::cout << std::endl;
	
	std::transform(sequence.begin(), sequence.end(), sequence.begin(), boost::bind(std::multiplies< double >(), acos(-1.0d), _1));
	std::copy(sequence.begin(), sequence.end(), std::ostream_iterator< double >(std::cout, "\n"));
	std::cout << std::endl;
	
	
	std::vector< boost::shared_ptr< Shape > > shape_list;
	boost::shared_ptr< Circle > Circle_1 = boost::make_shared< Circle >(Circle(rand() % 100 - 50, rand() % 100 - 50));
	boost::shared_ptr< Circle > Circle_2 = boost::make_shared< Circle >(Circle(rand() % 100 - 50, rand() % 100 - 50));
	boost::shared_ptr< Circle > Circle_3 = boost::make_shared< Circle >(Circle(rand() % 100 - 50, rand() % 100 - 50));
	
	boost::shared_ptr< Square > Square_1 = boost::make_shared< Square >(Square(rand() % 100 - 50, rand() % 100 - 50));
	boost::shared_ptr< Square > Square_2 = boost::make_shared< Square >(Square(rand() % 100 - 50, rand() % 100 - 50));
	boost::shared_ptr< Square > Square_3 = boost::make_shared< Square >(Square(rand() % 100 - 50, rand() % 100 - 50));

	boost::shared_ptr< Triangle > Triangle_1 = boost::make_shared< Triangle >(Triangle(rand() % 100 - 50, rand() % 100 - 50));
	boost::shared_ptr< Triangle > Triangle_2 = boost::make_shared< Triangle >(Triangle(rand() % 100 - 50, rand() % 100 - 50));
	boost::shared_ptr< Triangle > Triangle_3 = boost::make_shared< Triangle >(Triangle(rand() % 100 - 50, rand() % 100 - 50));

	shape_list.push_back(Circle_1);
	shape_list.push_back(Circle_2);
	shape_list.push_back(Circle_3);
	shape_list.push_back(Triangle_1);
	shape_list.push_back(Triangle_2);
	shape_list.push_back(Triangle_3);
	shape_list.push_back(Square_1);
	shape_list.push_back(Square_2);
	shape_list.push_back(Square_3);
	
	std::for_each(shape_list.begin(), shape_list.end(), shape_print());
	std::sort(shape_list.begin(), shape_list.end(), boost::bind(left_to_right(), _1, _2));
	std::cout << std::endl;
	std::for_each(shape_list.begin(), shape_list.end(), shape_print());

	std::sort(shape_list.begin(), shape_list.end(), boost::bind(left_to_right(), _2, _1));
	std::cout << std::endl;
	std::for_each(shape_list.begin(), shape_list.end(), shape_print());
	
	std::sort(shape_list.begin(), shape_list.end(), boost::bind(up_to_down(), _1, _2));
	std::cout << std::endl;
	std::for_each(shape_list.begin(), shape_list.end(), shape_print());
	
	std::sort(shape_list.begin(), shape_list.end(), boost::bind(up_to_down(), _2, _1));
	std::cout << std::endl;
	std::for_each(shape_list.begin(), shape_list.end(), shape_print());
	return 0;
}
