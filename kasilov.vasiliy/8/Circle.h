#ifndef CIRCLE_H
#define CIRCLE_H
#include "Shape.h"
class Circle: public Shape
{
	public:
		Circle(int new_x, int new_y);
		void draw() const;
};
#endif//CIRCLE_H
