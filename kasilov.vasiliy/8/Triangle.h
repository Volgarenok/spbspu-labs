#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "Shape.h"
class Triangle: public Shape
{
	public:
		Triangle(const int new_x, const int new_y);
		void draw() const;
};
#endif//TRIANGLE_H
