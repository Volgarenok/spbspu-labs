#ifndef FUNCTORS_8_H
#define FUNCTORS_8_H
#include <boost/smart_ptr/shared_ptr.hpp>
#include <cstdlib>
#include <ctime>
#include "Shape.h"
struct double_generator
{
	double operator()();
};

struct shape_print
{
	void operator()(const boost::shared_ptr< Shape > & shape_ptr);
};

struct left_to_right
{
	typedef bool result_type;
	result_type operator()(const boost::shared_ptr< Shape > & first_ptr, const boost::shared_ptr< Shape > & second_ptr);
};

struct up_to_down
{
	typedef bool result_type;
	result_type operator()(const boost::shared_ptr< Shape > & first_ptr, const boost::shared_ptr< Shape > & second_ptr);
};
#endif//FUNCTORS_8_H
