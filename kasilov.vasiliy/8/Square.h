#ifndef SQUARE_H
#define SQUARE_H
#include "Shape.h"
class Square : public Shape
{
	public:
		Square(const int new_x, const int new_y);
		void draw() const;
};
#endif//SQUARE_H
