#include "Functors_8.h"
double double_generator::operator()()
{
	return static_cast< double > (rand()) * 2 / RAND_MAX - 1;
}

void shape_print::operator()(const boost::shared_ptr< Shape > & shape_ptr)
{
	shape_ptr->draw();
}

left_to_right::result_type left_to_right::operator()(const boost::shared_ptr< Shape > & first_ptr, const boost::shared_ptr< Shape > & second_ptr)
{
	return first_ptr->isMoreLeft(*second_ptr);
}

up_to_down::result_type up_to_down::operator()(const boost::shared_ptr< Shape > & first_ptr, const boost::shared_ptr< Shape > & second_ptr)
{
	return first_ptr->isUpper(*second_ptr);
}	
