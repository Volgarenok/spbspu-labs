#include <list>//sequence_type
#include <algorithm>//std::for_each
#include <ctime>//srand
#include <iostream>
#include <iterator>
#include "Sequence_stat.h"//Sequence_stat
#include "Functors_7.h"
typedef std::list< int > sequence_type;
int main()
{
	srand(time(0));
	
	for(unsigned i = 0; i <= 13; i++)
	{
		sequence_type sequence;
		std::generate_n(std::insert_iterator< sequence_type >(sequence, sequence.begin()), i, sequence_random_generator());
		std::copy(sequence.begin(), sequence.end(), std::ostream_iterator< int >(std::cout, "\n"));			
		Sequence_stat stat;
		stat = std::for_each(sequence.begin(), sequence.end(), stat);
		stat.print();
		std::cout << "-----------------------------" << std::endl;
		sequence.clear();
	}
	return 0;
}
