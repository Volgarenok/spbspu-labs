#include "Functors_7.h"
#include "Sequence_stat.h"
#include <cstdlib>
sequence_random_generator::operator()()
{
	return rand() % (SEQ_CONST::LENGTH_GENERATION_BORDER) + SEQ_CONST::LEFT_SEQUENCE_BORDER;
}
