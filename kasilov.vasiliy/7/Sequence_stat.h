#ifndef SEQUENCE_STAT_H
#define SEQUENCE_STAT_H
namespace SEQ_CONST
{
	const int LEFT_SEQUENCE_BORDER = -500;
	const int LENGTH_GENERATION_BORDER = 1000;
}

class Sequence_stat
{
	public:
		Sequence_stat();
		void print() const;
		void operator()(int & value);
	
	private:
		int minimum;
		int maximum;

		float average;
		
		unsigned amount_of_positive;
		unsigned amount_of_negative;
		
		int summ_of_even;
		int summ_of_odd;
		
		bool front_back_equal;
	
		int front;
		int back;
		unsigned amount_of_sequence;
};
#endif//SEQUENCE_STAT_H
