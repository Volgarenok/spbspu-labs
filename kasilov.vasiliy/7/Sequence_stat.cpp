#include "Sequence_stat.h"
#include <iostream>//print console
#include <iomanip>//std::setw, std::boolalpha
#include <cstdlib>//rand
Sequence_stat::Sequence_stat():
	minimum(SEQ_CONST::LEFT_SEQUENCE_BORDER + SEQ_CONST::LENGTH_GENERATION_BORDER),
	maximum(SEQ_CONST::LEFT_SEQUENCE_BORDER),
	average(0),
	amount_of_positive(0),
	amount_of_negative(0),
	summ_of_even(0),
	summ_of_odd(0),
	front_back_equal(true),
	
	amount_of_sequence(0),
	front(0),
	back(0)
	{
		
	}

void Sequence_stat::print() const
{
	if(amount_of_sequence == 0)
	{
		std::cout << "DEFAULT TABLE STAT PACK" << "\n";		
	}
	std::cout << "---Stat generic pack---" << "\n";//23
	std::cout << "1>Maximum: " << std::setw(14) << maximum << "\n";
	std::cout << "2>Minimum: " << std::setw(14) << minimum << "\n";
	std::cout << "3>Average: " << std::setw(14) << average << "\n";
	std::cout << "4>Positive: " << std::setw(13) << amount_of_positive << "\n";
	std::cout << "5>Negative: " << std::setw(13) << amount_of_negative << "\n";
	std::cout << "6>Evens: " << std::setw(16) << summ_of_even << "\n";
	std::cout << "7>Odds: " << std::setw(17) << summ_of_odd << "\n";
	std::cout << "8>Front-back equal: " << std::setw(4) << std::boolalpha << front_back_equal << std::endl;		
}

void Sequence_stat::operator()(int & value)
{
	if(value >= maximum)
	{
		maximum = value;
	}
	
	if(value <= minimum)
	{
		minimum = value; 
	}
	
	if(value % 2)
	{
		summ_of_even += value;		
	}
	else
	{
		summ_of_odd += value;
	}
	
	if(value > 0)
	{
		amount_of_positive++;	
	}
	else
	if(value < 0)
	{
		amount_of_negative++;
	}
	amount_of_sequence++;
	
	average = static_cast< float > (summ_of_odd + summ_of_even) / static_cast< float > (amount_of_sequence);
	
	back = value;
	if(amount_of_sequence == 1)
	{
		front = value;
	}
	
	front_back_equal = front == back;
}
