#ifndef STRATEGY_ITER_HPP
#define STRATEGY_ITER_HPP
#include "Forward_list_it.hpp"
template < class Item >
struct StrategyIter
{
	typedef Item item_type;
	typedef Forward_list_it< item_type > store_type;
	typedef typename Forward_list_it< item_type >::iterator iterator;
	
	static iterator begin(store_type & store)
	{
		return store.begin();
	}
	
	static iterator end(store_type & store)
	{
		return store.end();
	}

	static item_type & get(store_type & store, iterator & iter)
	{
		return *iter;
	}
};
#endif//STRATEGY_ITER_HPP
