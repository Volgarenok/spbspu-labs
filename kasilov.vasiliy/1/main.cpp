#include "Base_strategy_functions.hpp"
#include "Strategy_Brackets.hpp"
#include "Strategy_At.hpp"
#include "Strategy_Iter.hpp"

#include <utility>
#include <vector> 
#include <iostream>
#include <stdexcept>
#include <ctime>
#include <cstdlib>
#include <fstream>

namespace ARRAY_CONST
{
	const int DEFAULT_ARRAY_CAPACITY = 10;
}

typedef std::pair< double * , size_t > double_array_pair_type;
typedef std::pair< char * , size_t > char_array_pair_type;
typedef std::vector< int > int_vector;
typedef std::vector< char > char_vector;
typedef std::vector< double > double_vector;
typedef const char* file_name_type;

char_array_pair_type read_array_pair_stream(file_name_type file_name)
{
	typedef char char_type;
	typedef char_type* char_type_ptr;
	typedef size_t count_type;
	typedef std::ifstream stream_type;
	
	if(!file_name)
	{
		throw std::invalid_argument("Empty file name");
	}
	
	stream_type input_stream(file_name);
	
	if(!input_stream)
	{
		input_stream.close();
		throw std::runtime_error("Fail instance input stream");
	}
		
	char_type_ptr char_array_ptr = static_cast< char_type_ptr > (malloc(ARRAY_CONST::DEFAULT_ARRAY_CAPACITY * sizeof(char_type)));
	if (!char_array_ptr)
	{
		input_stream.close();
		throw std::length_error("Out of memory");
	}
	
	count_type current_capacity = ARRAY_CONST::DEFAULT_ARRAY_CAPACITY;
	count_type current_size = 0;
	char_type current_symbol;
	
	while (input_stream.get(current_symbol))
	{
		if(current_size < current_capacity)
		{
			char_array_ptr[current_size] = current_symbol;
			current_size++;	
		}
		else
		{
			current_capacity = current_capacity * 16 / 10;
			char_array_ptr = static_cast< char_type_ptr >(realloc(char_array_ptr, current_capacity * sizeof(char_type)));
			if(!char_array_ptr)
			{
				input_stream.close();
				throw std::length_error("Out of memory");
			}
			input_stream.unget();
		}
	}
	
	if (!input_stream.eof())
	{
		input_stream.close();
		throw std::runtime_error("File stream crash");
	}

	char_array_ptr = static_cast< char_type_ptr >(realloc(char_array_ptr, current_size * sizeof(char_type)));
	if (!char_array_ptr)
	{
		throw std::length_error("Out of memory");
	}
	return char_array_pair_type (char_array_ptr, current_size);
}


void fill_random(const double_array_pair_type & array_pair)
{
	if (array_pair.second != 0 && array_pair.first != 0)
	{
		for(int i = 0; i < array_pair.second; i++)
		{
			array_pair.first[i] = static_cast< double > (rand() * 2 / RAND_MAX - 1);
		}
	}
	return;
}

template < class Item > 
std::pair < Item*, size_t > create_array_pair(size_t size)
{
	if (size != 0)
	{
		Item * item_array_ptr = static_cast< Item* > (malloc(size * sizeof(Item)));
	
		if (!item_array_ptr)
		{
			throw std::length_error("Out of memory");
		}
	
		return std::pair< Item*, size_t > (item_array_ptr, size);
	}
	return std::pair< Item*, size_t > (0, 0);
}

template < class Item >
void delete_array_pair(std::pair < Item*, size_t > & array_pair)
{
	if (array_pair.second != 0 && array_pair.first != 0)
	{
		free(array_pair.first);
		array_pair.first = 0;
		array_pair.second = 0;
	}
	return;
}

void edit_int_vector(int_vector & vect)
{
	if (!vect.empty())
	{
		int_vector::iterator i = vect.begin();
		int current_position = 0;
		
		if(vect.back() == 2)
		{
			while(i != vect.end())
			{
				if(*i % 3 == 0)
				{
					vect.insert(i + 1, 3, 1);//C++03 -> return void
					i = vect.begin();
					advance(i, current_position);			
				}
				current_position++;
				i++;	
			}
		}
		else
		if(vect.back() == 1)
		{
			while(i != vect.end())
			{
				if(*i % 2 == 0)
				{
					i = vect.erase(i);
				}
				else
				{
					current_position++;
					i++;					
				}
			}
		}
	}
}

int_vector read_int_vector_cin()
{
	int_vector input_vector;
	int temporary_int;
	while (std::cin >> temporary_int)
	{
		if (!std::cin)
		{
			throw std::runtime_error("Fail reading");
		}
		if (temporary_int != 0)
		{
			input_vector.push_back(temporary_int);
		}
		else
		{
			break;
		}
	}		
	return input_vector;
}

int main ()
{
	srand(time(0));
	//TASK 2
	char_array_pair_type TEST_char_ARRAY = read_array_pair_stream("input.txt");
	for(size_t i = 0; i < TEST_char_ARRAY.second; i++)
	{
		std::cout << TEST_char_ARRAY.first[i];
	}
	std::cout << TEST_char_ARRAY.second << std::endl;
	
	try
	{
		char_vector TEST_char_vector = char_vector(TEST_char_ARRAY.first, TEST_char_ARRAY.first + TEST_char_ARRAY.second);
		print< StrategyAt< char > >(TEST_char_vector);	
	}
	catch(...)
	{
		delete_array_pair< char >(TEST_char_ARRAY);
		throw std::length_error("TROUBLE");
	}
	delete_array_pair< char >(TEST_char_ARRAY);
	
	//TASK 4
	double_array_pair_type TEST_double_ARRAY = create_array_pair< double >(10);
	fill_random(TEST_double_ARRAY);
	for(size_t i = 0; i < TEST_double_ARRAY.second; i++)
	{
		std::cout << TEST_double_ARRAY.first[i] << std::endl;
	}
 	try
 	{
 		double_vector TEST_double_vector = double_vector(TEST_double_ARRAY.first, TEST_double_ARRAY.first + TEST_double_ARRAY.second);
 		print< StrategyAt< double > >(TEST_double_vector);
		sort < StrategyAt< double > >(TEST_double_vector);
		print< StrategyAt< double > >(TEST_double_vector);		
	}
	catch(...)
	{
		delete_array_pair< double >(TEST_double_ARRAY);
		throw std::length_error("TROUBLE");	
	}
	delete_array_pair< double >(TEST_double_ARRAY);

	//TASK 1
	int_vector TEST_vector;
	TEST_vector.push_back(3);
	TEST_vector.push_back(1);
	TEST_vector.push_back(2);
	TEST_vector.push_back(4);
	
	Forward_list_it< int > TEST_list(TEST_vector);
	print< StrategyIter< int > > (TEST_list);
	sort < StrategyIter< int > > (TEST_list);
	print< StrategyIter< int > > (TEST_list);
	
	TEST_vector.push_back(5);
	print< StrategyAt< int > > (TEST_vector);
	sort < StrategyAt< int > > (TEST_vector);
	print< StrategyAt< int > > (TEST_vector);
	
	TEST_vector.push_back(-5);
	TEST_vector.push_back(7);
	print< StrategyBrackets< int > > (TEST_vector);
	sort < StrategyBrackets< int > > (TEST_vector);
	print< StrategyBrackets< int > > (TEST_vector);
	
	//TASK 3
	TEST_vector.clear();
	TEST_vector = read_int_vector_cin();
	print< StrategyBrackets< int > > (TEST_vector);
	TEST_vector.push_back(3);
	TEST_vector.push_back(2);
	edit_int_vector(TEST_vector);
	print< StrategyBrackets< int > > (TEST_vector);
	TEST_vector.push_back(2);
	TEST_vector.push_back(1);
	edit_int_vector(TEST_vector);
	print< StrategyBrackets< int > > (TEST_vector);
	return 0;
}
