#ifndef STRATEGY_BRACKETS_HPP
#define STRATEGY_BRACKETS_HPP
#include <vector>
template < class Item >
struct StrategyBrackets
{
	typedef Item item_type;
	typedef typename std::vector< item_type > store_type;
	typedef unsigned int iterator;	
	
	static iterator begin(store_type & store)
	{
		return 0;
	}
	
	static iterator end(store_type & store)
	{
		return store.size();
	}

	static item_type & get(store_type & store, iterator & iter)
	{
		return store[iter];
	}
};
#endif//STRATEGY_BRACKETS_HPP
