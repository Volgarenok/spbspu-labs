#ifndef FORWARD_LIST_IMPLEMENT_H
#define	FORWARD_LIST_IMPLEMENT_H
template < class Item >
struct forward_list_implement
{
	typedef Item item_type;
	typedef forward_list_implement< item_type > this_type;
	typedef this_type * this_type_ptr;
	
	item_type value;
	this_type_ptr next_node;
};
#endif//FORWARD_LIST_TMPLEMENT_H
