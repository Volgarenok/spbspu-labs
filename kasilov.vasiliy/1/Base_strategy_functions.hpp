#ifndef BASE_STRATEGY_FUNCTIONS_HPP
#define BASE_STRATEGY_FUNCTIONS_HPP
#include <iostream>
template < class Strategy >
void sort (typename Strategy::store_type & store)
{
	if (!store.empty())
	{
		for ( typename Strategy::iterator i(Strategy::begin(store)); i != Strategy::end(store); i++ )
		{
			for ( typename Strategy::iterator j(i); j != Strategy::end(store); j++)
			{
				if ( Strategy::get(store, i) < Strategy::get(store, j))
				{
					std::swap(Strategy::get(store, i), Strategy::get(store, j));
				}
			}
		}	
	}
}

template < class Strategy >
void print(typename Strategy::store_type & store)
{
	if (!store.empty())
	{
		for ( typename Strategy::iterator i(Strategy::begin(store)); i != Strategy::end(store); i++ )
		{
			std::cout << Strategy::get(store, i) << "\n";
		}
		std::cout << std::endl;
	}
}
#endif//BASE_STRATEGY_FUNCTIONS_HPP
