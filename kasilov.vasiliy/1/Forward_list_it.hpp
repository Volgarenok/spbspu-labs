#ifndef FORWARD_LIST_IT_HPP
#define	FORWARD_LIST_IT_HPP
#include "Forward_list_implement.h"
#include <iterator>
#include <vector>
#include <cassert>
#include <stdexcept>
template < class Item >
class Forward_list_it
{
	public:
		typedef Item item_type;
		typedef typename forward_list_implement< item_type >::this_type node_type;
		typedef typename forward_list_implement< item_type >::this_type_ptr node_type_ptr;
		
		class Forward_iterator: public std::iterator < std::forward_iterator_tag, item_type >
		{
			public:
				typedef Item item_type;
				typedef Forward_iterator this_type;
				typedef typename forward_list_implement< item_type >::this_type node_type;
				typedef typename forward_list_implement< item_type >::this_type_ptr node_type_ptr;
			
				Forward_iterator();
				Forward_iterator(node_type_ptr node);
				
				bool operator ==(const this_type & iter) const;
				bool operator !=(const this_type & iter) const;
			
				item_type & operator  *() const;
				item_type * operator ->() const;
			
				this_type & operator ++();
				this_type operator ++(int);
			
			private:
				node_type_ptr _node;
		};
		
		typedef typename Forward_iterator::this_type iterator;
		typedef typename std::vector< item_type > init_container_type;
		typedef Forward_list_it< item_type > this_type;
		
		Forward_list_it();
		Forward_list_it(node_type head);
		Forward_list_it(const init_container_type & vect);
		
		~Forward_list_it();
		void clear();
		
		bool empty() const;
		
		iterator begin() const;
		iterator end() const;
		
		bool operator==(const this_type & list);
		bool operator!=(const this_type & list);
		
	private:
		node_type_ptr _head;
		
		Forward_list_it(const this_type &);
		this_type & operator= (const this_type &);
};

template < class Item >
Forward_list_it< Item >::Forward_iterator::Forward_iterator():
	_node(0)
	{
		
	}

template < class Item >
Forward_list_it< Item >::Forward_iterator::Forward_iterator(node_type_ptr node):
	_node(node)
	{
		
	}

template < class Item >
bool Forward_list_it< Item >::Forward_iterator::operator==(const this_type & iter) const
{
	return iter._node == this->_node;
}

template < class Item >
bool Forward_list_it< Item >::Forward_iterator::operator!=(const this_type & iter) const
{
	return iter._node != this->_node;
}

template < class Item >
typename Forward_list_it< Item >::Forward_iterator::item_type & Forward_list_it< Item >::Forward_iterator::operator*() const
{
	assert(this->_node != 0);
	return this->_node->value;
}

template < class Item >
typename Forward_list_it< Item >::Forward_iterator::item_type * Forward_list_it< Item >::Forward_iterator::operator->() const
{
	assert(this->_node != 0);
	return &this->_node->value;
}

template < class Item >
typename Forward_list_it< Item >::Forward_iterator & Forward_list_it< Item >::Forward_iterator::operator ++()
{
	assert(this->_node != 0);
	this->_node = this->_node->next_node;
	return *this;
}

template < class Item >
typename Forward_list_it< Item >::Forward_iterator Forward_list_it< Item >::Forward_iterator::operator ++(int)
{
	this_type temp (*this);
	this -> operator ++();
	return temp;
}

template < class Item >
Forward_list_it< Item >::Forward_list_it(const init_container_type & vect)
{
	if(!vect.empty())
	{
		try
		{
			_head = new node_type;		
			_head->value = vect.front();
			_head->next_node = 0;
		
			node_type_ptr temp(_head);
		
			for(int i = 1; i < vect.size(); i++)
			{
				assert(temp != 0);
				temp->next_node = new node_type;		
			
				temp->next_node->value = vect[i];
				temp->next_node->next_node = 0;
				temp = temp->next_node;
			}			
		}
		catch(...)
		{
			clear();
			throw std::length_error("Out of memory");
		}		
	}
	else
	{
		_head = 0;
	}
}

template < class Item >
Forward_list_it< Item >::Forward_list_it():
	_head(0)
		{

		}

template < class Item >
Forward_list_it< Item >::Forward_list_it(node_type head):
	_head(head)
	{
	
	}

template < class Item >
void Forward_list_it< Item >::clear()
	{
		while (_head != 0)
		{
			node_type_ptr temp(_head);
			_head = _head->next_node;
			delete temp;
		}
	}

template < class Item >
Forward_list_it< Item >::~Forward_list_it()
	{
		clear();
	}

template < class Item >
typename Forward_list_it< Item >::iterator Forward_list_it< Item >::begin() const
{
	return _head;
}

template < class Item >
typename Forward_list_it< Item >::iterator Forward_list_it< Item >::end() const
{
	return 0;
}

template < class Item >
bool Forward_list_it< Item >::empty() const
{
	return _head == 0;
}
template < class Item >
bool Forward_list_it< Item >::operator==(const this_type & list)
{
	return list.begin() == this->begin();
}

template < class Item >
bool Forward_list_it< Item >::operator!=(const this_type & list)
{
	return list.begin() != this->begin();
}
#endif//FORWARD_LIST_IT_HPP


