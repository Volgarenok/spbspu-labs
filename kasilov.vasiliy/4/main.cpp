#include "Book.h"
#include "Factorial_container.h"
#include <iostream>
#include <vector>
#include <algorithm>
int main ()
{
	//TASK 1
	std::cout << "//Task 1" << std::endl;
	Book::person np1 = {"FIO_1", "num: 1111"};
	Book::person np2 = {"FIO_2", "num: 2222"};
	Book::person np3 = {"FIO_3", "num: 3333"};
	Book::person npx1 = {"1xxxx", "num: xxxx"};
	Book::person npx2 = {"2xxxx", "num: xxxx"};	
	Book::person npx3 = {"3xxxx", "num: xxxx"};		
	
	Book my_book;
	Book::iterator iter = my_book.begin();
	my_book.put(iter, np1);
	my_book.put(iter, np2);
	my_book.put(iter, np3);
	my_book.jump(iter, 2);
	my_book.put(iter, npx1);
	my_book.put(iter, npx2);
	my_book.put(iter, npx3);
	my_book.jump(iter);
	my_book.modify(iter, np2);
	Book::insert_iterator inser = my_book.insertly(iter);
	my_book.modify(inser, np3);
	my_book.put(iter, np3);
	
	
	for(Book::iterator i = my_book.begin(); i != my_book.end(); i++)
	{
		std::cout << i->fio << " " << i->phone << std::endl;
	}
	
	
	//TASK 2
	std::cout << "//Task 2" << std::endl;
	Factorial_container TEST_fact(10);
	
	std::copy(TEST_fact.begin(), TEST_fact.end(), std::ostream_iterator< Factorial_container::value_type >(std::cout, "\n"));
		
	std::vector< Factorial_container::value_type > test_vector;
	std::copy(TEST_fact.begin(), TEST_fact.end(), std::inserter(test_vector, test_vector.begin()));
	
	std::cout << std::endl;
	std::copy(test_vector.begin(), test_vector.end(), std::ostream_iterator< Factorial_container::value_type >(std::cout, "\n"));
	return 0;
}
