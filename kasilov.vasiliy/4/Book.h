#ifndef BOOK_H
#define BOOK_H
#include <list>
#include <string>
class Book
{
	public:
		struct person
		{
			typedef std::string value_type;
			value_type fio;
			value_type phone;
		};
		typedef person item_type;
		typedef std::list< item_type > container_type; //list/vector
		typedef typename container_type::iterator iterator;
		typedef typename std::insert_iterator< container_type > insert_iterator;
		
		iterator begin();
		iterator end();
		
		insert_iterator insertly(iterator & iter);
		
		void put(iterator & iter, const item_type & record);
		void clear();	
		void jump(iterator & iter, const int & n = 1);	
		bool empty() const;	
		
		template < class Iterator >
		void modify(Iterator & modify_position, const item_type & record);
			
	private:
		container_type storage;
};

template < class Iterator >
void Book::modify(Iterator & modify_position, const item_type & record)
{
	*modify_position = record;
}
#endif//BOOK_H
