#include "Book.h"
typename Book::iterator Book::begin()
{
	return storage.begin();
}

typename Book::iterator Book::end()
{
	return storage.end();
}

typename Book::insert_iterator Book::insertly(iterator & iter)
{
	return std::inserter(storage, iter);
}

void Book::clear()
{
	storage.clear();
}

bool Book::empty() const
{
	return storage.empty();
}

void Book::put(iterator & iter, const item_type & record)
{
	iter = storage.insert(iter, record);
}

void Book::jump(iterator & iter, const int & n)
{		
	advance(iter, n);
	if(iter == end())
	{
		iter = begin();
	}
}
