#ifndef FACTORIAL_CONTAINER_H
#define FACTORIAL_CONTAINER_H
#include <iterator>
class Factorial_container
{
	public:
		typedef unsigned int value_type;
		class Factorial_iterator: public std::iterator < std::bidirectional_iterator_tag, value_type >
		{
		public:
			typedef Factorial_iterator this_type;
			typedef value_type item_type;
		
			Factorial_iterator();
			Factorial_iterator(item_type position, item_type new_border);
		
			bool operator ==(const this_type & iter) const;
			bool operator !=(const this_type & iter) const;
			
			item_type operator  *() const;
			
			this_type & operator ++();
			this_type operator ++(int);
			
			this_type & operator --();
			this_type operator --(int);
									  		
		private:
			item_type factorial;
			unsigned current;
			
			unsigned border;
		};
		
		typedef Factorial_iterator iterator;
		
		
		Factorial_container();
		Factorial_container(unsigned new_size);
		
		iterator begin() const;
		iterator end() const;
		
		static value_type fact(const value_type & number)
		{
			value_type return_value = 1;
			for(value_type i = 1; i <= number; i++)
			{
				return_value *= i;
			}
			return return_value;
		}
	
	private:
		unsigned size;
};
#endif//FACTORIAL_CONTAINER_H
