#include "Factorial_container.h"
#include <cassert>
Factorial_container::Factorial_iterator::Factorial_iterator():
	current(1),
	factorial(1),
	border(10)
	{
		
	}

Factorial_container::Factorial_iterator::Factorial_iterator(item_type position, item_type new_border):
	current(position),
	border(new_border)
	{
		factorial = Factorial_container::fact(position);
	}
	
bool Factorial_container::Factorial_iterator::operator==(const this_type & iter) const
{
	return iter.current == this->current;
}

bool Factorial_container::Factorial_iterator::operator!=(const this_type & iter) const
{
	return iter.current != this->current;
}

typename Factorial_container::Factorial_iterator::item_type Factorial_container::Factorial_iterator::operator*() const
{
	assert(current != border);
	return factorial;
}

Factorial_container::Factorial_iterator & Factorial_container::Factorial_iterator::operator ++()
{
	assert(current != border);
	factorial *= ++current;
	return *this;
}

Factorial_container::Factorial_iterator::this_type Factorial_container::Factorial_iterator::operator ++(int)
{
	this_type temp (*this);
	this -> operator ++();
	return temp;
}

Factorial_container::Factorial_iterator::this_type & Factorial_container::Factorial_iterator::operator--()
{
	assert(current != 1);
	factorial /= current--;
	return *this;
}

typename Factorial_container::Factorial_iterator::this_type Factorial_container::Factorial_iterator::operator--(int)
{
	this_type temp (*this);
	this -> operator --();
	return temp;
}	
