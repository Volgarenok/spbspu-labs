#include <algorithm>//for_each, sort
#include <iostream>//print console
#include <iterator>
#include <sstream>
#include <vector>//container_type
#include <string>//string type
#include <ctime>//random generate
#include <cstdlib>//^^^
#include <iomanip>
#include <boost/bind.hpp>
typedef std::string string_type;
namespace LAB_CONST
{
	const int AMOUNT_OF_STRINGS_IN_TABLE = 10;
	const int RIGHT_RANDOM_LENGTH = 10;
	const int LEFT_RANDOM_BORDER = -5;
}

struct data_struct
{	
	int key1;
	int key2;
	string_type str;
	friend std::ostream & operator << (std::ostream & out, const data_struct & out_data)
	{
		out << std::setw(4) << std::right << out_data.key1;
		out << std::setw(4) << std::right << out_data.key2;
		out << std::setw(10) << std::right << out_data.str;	
	}
	
};
typedef std::vector< data_struct > container_type;
typedef std::vector< string_type > table_type;

struct random_vector_generator
{
	typedef data_struct result_type;
	data_struct operator()(table_type & table)
	{
		data_struct new_struct;
		new_struct.key1 = rand() % (LAB_CONST::RIGHT_RANDOM_LENGTH + 1) + LAB_CONST::LEFT_RANDOM_BORDER;
		new_struct.key2 = rand() % (LAB_CONST::RIGHT_RANDOM_LENGTH + 1) + LAB_CONST::LEFT_RANDOM_BORDER;
		new_struct.str = table[rand() % table.size()];	
		return new_struct;	
	}
};

struct criterion_sorter
{
	bool operator()(const data_struct & first, const data_struct & second)
	{
		return (first.key1 < second.key1) ||
		(first.key1 == second.key1 && first.key2 < second.key2) ||
		(first.key2 == second.key2 && first.str.length() < second.str.length());
	}
};


struct table_generator
{
	typedef string_type result_type;
	string_type operator()(int & value)
	{
		std::stringstream str_stream;
		str_stream << value;
		string_type str;
		str_stream >> str;
		int length_gener = value % 5;
		for(int i = 0; i < length_gener; i++)
		{
			str = "_" + str;
		}
		value++;
		return "s__" + str;
	}
};

int main ()
{
	srand(time(0));
	table_type new_table;
	std::generate_n(std::inserter(new_table, new_table.begin()), 10, boost::bind(table_generator(), 0));
	std::cout << "Gener table" << std::endl;
	std::copy(new_table.begin(), new_table.end(), std::ostream_iterator< string_type >(std::cout, "\n"));
	std::random_shuffle(new_table.begin(), new_table.end());
	std::cout << "Random shuffle table" << std::endl;
	std::copy(new_table.begin(), new_table.end(), std::ostream_iterator< string_type >(std::cout, "\n"));
	
	container_type test_vector;
	std::generate_n(std::inserter(test_vector, test_vector.begin()), 100, boost::bind(random_vector_generator(), new_table));
	std::cout << "---Gener structs---" << std::endl;
	std::copy(test_vector.begin(), test_vector.end(), std::ostream_iterator< data_struct >(std::cout, "\n"));
	
	std::cout << "\n";
	std::cout << "+++sorted+++" << std::endl;
	
	std::sort(test_vector.begin(), test_vector.end(), criterion_sorter());
	std::copy(test_vector.begin(), test_vector.end(), std::ostream_iterator< data_struct >(std::cout, "\n"));
	return 0;
}
