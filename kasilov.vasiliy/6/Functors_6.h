#ifndef FUNCTORS_6_H
#define FUNCTORS_6_H
#include "Shape_generator.hpp"
struct shape_random_generator
{
	shapespace::Shape operator()();
};

struct shape_vertex_accumulator
{
	int operator()(int last_sum, const shapespace::Shape & new_shape);
};

struct type_checker
{
	typedef bool result_type;
	bool operator()(const shapespace::Shape & new_shape, const shapespace::Shape::form_type & category);
};

struct shape_point_transformer
{
	typedef shapespace::Shape::point result_type;
	result_type operator()(const shapespace::Shape & new_shape);
};

struct shape_sorting_functor
{
	bool operator()(const shapespace::Shape & first, const shapespace::Shape & second);
};
#endif//FUNCTORS_6_H
