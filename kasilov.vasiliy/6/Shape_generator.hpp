#ifndef SHAPE_GENERATOR_HPP
#define SHAPE_GENERATOR_HPP

#include "Shape.h"//shape
#include <cstdlib>//random
#include <iomanip>//maip stream
namespace GENER_CONST
{
	const int COORD_GENERATION_BORDER_LENGTH = 200;
	const int COORD_GENERATION_BORDER_MIN = -100;
	const int LENGTH_GENERATION_BORDER = 20;
}

struct shape_generator
{
	typedef unsigned width_height_type;
	typedef shapespace::Shape shape_type;
	
	static shape_type generate_rectangle()
	{
		shapespace::Shape::point new_point = generate_point();
		width_height_type width = generate_length(); //!= 0
		width_height_type height = generate_length();
	
		if (width == height)
		{
			height++;
		}
		
		shape_type new_shape;
		new_shape.push_point(new_point);//point: (x,y)
		
		new_point.x = new_point.x + width;
		new_shape.push_point(new_point);//point: (x + width, y)
		
		new_point.y = new_point.y + height;
		new_point.x = new_point.x - width;
		new_shape.push_point(new_point);//point: (x, y + height)
		
		new_point.x = new_point.x + width;
		new_shape.push_point(new_point);//point: (x + width, y + height)

		return new_shape;
	}
	
	static shape_type generate_square()
	{
		shapespace::Shape::point new_point = generate_point();
		width_height_type width = generate_length(); //!= 0
		
		shape_type new_shape;
		new_shape.push_point(new_point);//point: (x, y)
		
		new_point.x = new_point.x + width;
		new_shape.push_point(new_point);//point: (x + width, y)
		
		new_point.y = new_point.y + width;
		new_point.x = new_point.x - width;
		new_shape.push_point(new_point);//point: (x, y + width)
		
		new_point.x = new_point.x + width;
		new_shape.push_point(new_point);//point: (x, y + height)

		return new_shape;
	}

	
	static shape_type generate_triangle()
	{
		shape_type new_shape;
		for(unsigned i = 0; i < 3; i++)
		{
			new_shape.push_point(generate_point());
		}
		return new_shape;
	}
	
	static shape_type generate_pentagon()
	{
		shape_type new_shape;
		for(unsigned i = 0; i < 5; i++)
		{
			new_shape.push_point(generate_point());
		}
		return new_shape;		
	}
	
	
	private: static shapespace::Shape::point generate_point()
	{
		shapespace::Shape::point new_point;
		new_point.x = rand() % (GENER_CONST::COORD_GENERATION_BORDER_LENGTH + 1) + GENER_CONST::COORD_GENERATION_BORDER_MIN;
		new_point.y = rand() % (GENER_CONST::COORD_GENERATION_BORDER_LENGTH + 1) + GENER_CONST::COORD_GENERATION_BORDER_MIN;
		return new_point;
	}
	
	private: static width_height_type generate_length()
	{
		return rand() % (GENER_CONST::LENGTH_GENERATION_BORDER) + 1;
	}
	
};
#endif//SHAPE_GENERATOR_HPP
