#include "Shape.h"
#include "io_state_guard.h"
#include <iomanip>
void shapespace::Shape::push_point(const point & new_point)
{
	vertexec.push_back(new_point);
	shape_category = NOT_A_SHAPE;
}
	
shapespace::Shape::form_type shapespace::Shape::get_category() const
{
	if (get_vertex_num() == 3)
	{
		return Shape::TRIANGLE;
	}
	else
	if (get_vertex_num() == 5)
	{
		return Shape::PENTAGON;
	}
	else
	if (get_vertex_num() == 4)
	{
		if(vertexec[1].x - vertexec[0].x == vertexec[2].y - vertexec[0].y)
		{
			return Shape::SQUARE;
		}
		else
		{
			return Shape::RECTANGLE;
		}
	}
	else
	{
		return Shape::NOT_A_SHAPE;
	}
}
		
shapespace::Shape::point_type shapespace::Shape::operator[](const unsigned i) const
{
	return vertexec[i];
}
		
unsigned shapespace::Shape::get_vertex_num() const
{
	return vertexec.size();
}

std::ostream & shapespace::operator<< (std::ostream & out, const Shape & out_shape)
{
	std::ostream::sentry sentry(out);
	if (sentry)
	{
		::io_state_guard state(out);
		
		out << "(:shape_type\"";
		if(out_shape.get_category() == Shape::TRIANGLE)
		{
			out << "TRIANGLE";
		}
		else
		if(out_shape.get_category() == Shape::PENTAGON)
		{
			out << "PENTAGON";	
		}
		else
		if(out_shape.get_category() == Shape::SQUARE)
		{
			out << "SQUARE";
		}
		else
		if(out_shape.get_category() == Shape::RECTANGLE)
		{
			out << "RECTANGLE";
		}
		else
		{
			out << "NOT_A_SHAPE";
		}
		out << "\")" << "\n";
		for (unsigned i = 0; i < out_shape.get_vertex_num(); i++)
		{
			out << out_shape[i];
		}
		out << std::endl;		
	}
	return out;			
}
	
std::ostream & shapespace::operator<< (std::ostream & out, const Shape::point & out_point)
{
	std::ostream::sentry sentry(out);
	if (sentry)
	{
		::io_state_guard state(out);
		
		out << "(:x " << std::setw(3) << out_point.x;
		out << " ";
		out << ":y "  << std::setw(3) << out_point.y;
		out << ")" << "\n";
	}
	return out;		
}	
