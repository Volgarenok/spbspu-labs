#ifndef SHAPE_H
#define SHAPE_H
#include <vector>
#include <ostream>
#include <iomanip>
namespace shapespace
{
	class Shape
	{
		public:
			enum form_type
			{
				TRIANGLE,
				RECTANGLE,
				SQUARE,
				PENTAGON,
				NOT_A_SHAPE
			};
			
			struct point
			{		
				int x;
				int y;
			};
	
			typedef point point_type;
			typedef std::vector< point_type > container_type;

			void push_point(const point & new_point);	
			form_type get_category() const;
			point_type operator[](const unsigned i) const;
			unsigned get_vertex_num() const;
				
		private:
			form_type shape_category;
			container_type vertexec;
	};

	std::ostream & operator<< (std::ostream & out, const Shape & out_shape);
	std::ostream & operator<< (std::ostream & out, const Shape::point & out_point);
}
#endif//SHAPE_H
