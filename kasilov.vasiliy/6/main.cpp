#include "Functors_6.h"
#include <boost/bind.hpp>
#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <numeric>
#include <iterator>
#include <fstream>
#include <set>
typedef std::vector< shapespace::Shape > shape_container;

int main()
{
	//First task
	std::ifstream stream("input.txt");
	try
	{
		std::set< std::string > test_set = std::set< std::string >(std::istream_iterator< std::string >(stream), std::istream_iterator< std::string >());		
		std::copy(test_set.begin(), test_set.end(), std::ostream_iterator< std::string >(std::cout, "\n"));
	}
	catch(...)
	{
		throw std::runtime_error("Stream error");
	}
	
	
	
	//Second task
	
	//Fill random
	srand(time(0));
	shape_container TEST;
	std::generate_n(std::insert_iterator< shape_container >(TEST, TEST.begin()), 5, shape_random_generator());
	std::copy(TEST.begin(), TEST.end(), std::ostream_iterator< shapespace::Shape >(std::cout));
	
	//Summ
	std::cout << "Summ vertex numeric: " << std::accumulate(TEST.begin(), TEST.end(), 0, shape_vertex_accumulator()) << std::endl;

	//Rect
	std::cout << "Amount of RECTANGLEs: " << std::count_if(TEST.begin(), TEST.end(), boost::bind(type_checker(), _1, shapespace::Shape::RECTANGLE)) << std::endl;
	//Square
	std::cout << "Amount of SQUAREs: " << std::count_if(TEST.begin(), TEST.end(), boost::bind(type_checker(), _1, shapespace::Shape::SQUARE)) << std::endl;
	//Triangle
	std::cout << "Amount of TRIANGLEs: " << std::count_if(TEST.begin(), TEST.end(), boost::bind(type_checker(), _1, shapespace::Shape::TRIANGLE)) << std::endl;
	//Pentagon
	std::cout << "Amount of PENTAGONs: " << std::count_if(TEST.begin(), TEST.end(), boost::bind(type_checker(), _1, shapespace::Shape::PENTAGON)) << std::endl;
	
	std::cout << std::endl;
	TEST.erase(std::remove_if(TEST.begin(), TEST.end(), boost::bind(type_checker(), _1, shapespace::Shape::PENTAGON)), TEST.end());
	std::copy(TEST.begin(), TEST.end(), std::ostream_iterator< shapespace::Shape >(std::cout));

	//Front-points
	std::vector< shapespace::Shape::point > TEST_point;
	std::transform(TEST.begin(), TEST.end(), std::insert_iterator< std::vector< shapespace::Shape::point > >(TEST_point, TEST_point.begin()),shape_point_transformer());
	std::cout << "Pointers:" << std::endl;
	std::copy(TEST_point.begin(), TEST_point.end(), std::ostream_iterator< shapespace::Shape::point >(std::cout));
	std::cout << std::endl;
	
	//Sort (enum)
	std::cout << "Sorted shapes:" << std::endl;
	std::sort(TEST.begin(), TEST.end(), shape_sorting_functor());
	std::copy(TEST.begin(), TEST.end(), std::ostream_iterator< shapespace::Shape >(std::cout));
	std::cout << std::endl;
	return 0;
}
