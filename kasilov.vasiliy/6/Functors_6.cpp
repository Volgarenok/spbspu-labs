#include "Functors_6.h"
using namespace shapespace;
Shape shape_random_generator::operator()()
{
	unsigned shape_number = rand() % 4;
	switch (shape_number)
	{
		case 0:
			return shape_generator::generate_triangle();
		case 1:
			return shape_generator::generate_rectangle();
		case 2:
			return shape_generator::generate_square();
		case 3:
			return shape_generator::generate_pentagon();
	}			
}

int shape_vertex_accumulator::operator()(int last_sum, const Shape & new_shape)
{
	return last_sum + new_shape.get_vertex_num();		
}

type_checker::result_type type_checker::operator()(const Shape & new_shape, const Shape::form_type & category)
{
	return new_shape.get_category() == category;
}


shape_point_transformer::result_type shape_point_transformer::operator()(const Shape & new_shape)
{
	return new_shape[1];
}

bool shape_sorting_functor::operator()(const Shape & first, const Shape & second)
{
	return first.get_category() < second.get_category();
}
