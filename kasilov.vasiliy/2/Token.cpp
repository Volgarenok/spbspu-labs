#include "token.h"
void token::set_value(const format_type & new_value)
{
	token_value = new_value;
}
	
unsigned int token::get_size()
{
	return token_value.length();
}
	
token::format_type token::get_value()
{
	return token_value;
}
	
token::token_type_enum token::get_category()
{
	return token_category;
}
