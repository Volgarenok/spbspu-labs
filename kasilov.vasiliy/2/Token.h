#ifndef TOKEN_H
#define TOKEN_H
#include <vector>//container_type that FSM will use
#include <string>//format_type that use for text complement
struct token
{
	typedef enum
	{
		WORD,
		PUNCT
	} token_type_enum;
	
	typedef char elementary_type;
	typedef std::string format_type;
	typedef std::vector< token > container_type;
	typedef std::vector< format_type > text_format_type;

	void set_value(const format_type & new_value);
	unsigned int  get_size();
	format_type get_value();	
	token_type_enum get_category();
	
	friend std::ostream & operator<< (std::ostream & out, const token & out_token)
	{
		out << out_token.token_value;
		return out;
	}

	format_type token_value;
	token_type_enum token_category;
};
#endif//TOKEN_H
